import os
import numpy as np
from keras.preprocessing import image
from keras.applications.vgg19 import preprocess_input
from utils.pyflow.pyflow import coarse2fine_flow
import utils.flowshift as flowshift
import pickle
import feature_extract
from scipy.misc import imsave
from utils.matimresize import imresize
from utils.ANN import *
import glob

def list_dirs(root):
    return os.listdir(root)

def optical_flow(root,):
    alpha = 0.012
    ratio = 0.75
    minWidth = 20
    nOuterFPIterations = 7
    nInnerFPIterations = 1
    nSORIterations = 30
    colType = 0  # 0 or default:RGB, 1:GRAY (but pass gray image with shape (h,w,1))

    frames_l = os.listdir(root)
    frames_l.sort()
    img_path = root + '/'+ frames_l[0]
    curr = image.load_img(img_path)
    curr = image.img_to_array(curr)
    curr = curr.astype(float) / 255
    h = curr.shape[0]
    w = curr.shape[1]
    t = 0
    flow_struct = {}
    flow_struct['u'] = list()
    flow_struct['v'] = list()
    for frame_s in frames_l[1:len(frames_l)]:
            prev=curr
            img_path = root  + '/'+ frame_s
            curr = image.load_img(img_path)
            curr = image.img_to_array(curr)
            curr = curr.astype(float) / 255
            u, v, im2W = coarse2fine_flow(
                prev, curr, alpha, ratio, minWidth, nOuterFPIterations, nInnerFPIterations,
                nSORIterations, colType)
            u = np.float32(np.expand_dims(u,axis=0))
            v = np.float32(np.expand_dims(v,axis=0))
            flow_struct['u'].append(u)
            flow_struct['v'].append(v)
    flow_struct['u'] = np.concatenate(flow_struct['u'])
    flow_struct['v'] = np.concatenate(flow_struct['v'])
    orig= flowshift.origin_by_flow(flow_struct,h,w)
    flow_struct=flowshift.expand_flow(flow_struct,orig)
    #flow_struct['orig']=orig
    return flow_struct

def imresize_and_save(root,vid,scale=2):
    frames = list()
    frames_l = os.listdir(root + "/" + vid)
    t = 0
    vid_struct = {}
    for frame_s in frames_l:
        img_path = root + '/' + vid + '/' + frame_s
        img = image.load_img(img_path)
        x = image.img_to_array(img, )
        shape = img.size
        x=imresize(x,1/scale)
        imsave(img_path,x)

def anot_resize_and_save(root,vid,scale=2):
    objects_gt = list_dirs(root+vid)

    for i in range(len(objects_gt)):
        frames_l = os.listdir(root+vid + '/' + objects_gt[i])
        for j in range(len(frames_l)):
            img_path = root+vid + '/' + objects_gt[i] + '/' + frames_l[j]
            img = image.load_img(img_path)
            x = image.img_to_array(img, )
            shape = img.size
            x=imresize(x,1/scale)
            x[x>=1]=255
            x[x < 1] = 0
            imsave(img_path,x)


def preprocess_to_net_in_single(root,vid,net_shape):
    frames = list()
    frames_l = glob.glob(root + "/" + vid+'/*.bmp')
    frames_l += glob.glob(root + "/" + vid+'/*.png')

    t = 0
    vid_struct = {}
    for frame_s in frames_l:
        img_path = frame_s
        img = image.load_img(img_path)
        shape = img.size
        img = image.load_img(img_path, target_size=net_shape)
        x = image.img_to_array(img, )
        x = np.expand_dims(x, axis=0)
        x = preprocess_input(x)
        frames.append(x)
    vid_struct['frames'] = np.concatenate(frames, 0)
    vid_struct['shape'] = (shape[1],shape[0])
    return vid_struct

def preprocess_full(root,net_shape,save = False,optflow=False):
    dataset = root+'Images/'
    vid_s = list_dirs(dataset)
    for vid in vid_s:
        vid_struct  = preprocess_to_net_in_single(dataset,vid,net_shape)
    #    vid_struct['l'] = create_annotations(root+'Annotations/'+vid,vid_struct['shape'],len(vid_struct['frames']))
        if optflow:
            flow_struct = optical_flow(dataset+'/'+vid)
            vid_struct['u'] = flow_struct['u']
            vid_struct['v'] = flow_struct['v']
        #vid_struct['orig'] = flow_struct['orig']
        if save:
            out_dir = root+'preprocessed/'
            if not(os.path.isdir(out_dir)):
                os.mkdir(out_dir)
            out_file = out_dir+vid+'.npy'
            if os.path.exists(out_file):
                os.remove(out_file)
            fp = open(out_dir+vid+'.npy',"wb")
            pickle.dump(vid_struct,fp)
            fp.close()

def preprocess_single(root,vid,net_shape,save = False,optflow=False):
    dataset = root+'Images/'
    vid_struct  = preprocess_to_net_in_single(dataset,vid,net_shape)
    #vid_struct['l'] = create_annotations(root+'Annotations/'+vid,vid_struct['shape'],len(vid_struct['frames']))
    if optflow:
            flow_struct = optical_flow(root)
            vid_struct['u'] = flow_struct['u']
            vid_struct['v'] = flow_struct['v']
        #vid_struct['orig'] = flow_struct['orig']
    if save:
        out_dir = root+'/preprocessed/'
        if not(os.path.isdir(out_dir)):
            os.mkdir(out_dir)
        out_file = out_dir+vid+'.npy'
        if os.path.exists(out_file):
            os.remove(out_file)
        fp = open(out_dir+vid+'.npy',"wb")
        pickle.dump(vid_struct,fp)
        fp.close()
    return vid_struct

def create_annotations(vid_root,shape,frames):
    objects_gt = list_dirs(vid_root)
    labels = np.zeros((frames, shape[0], shape[1]), dtype=int)

    for i in range(len(objects_gt)):
        frames_l = os.listdir(vid_root+'/'+objects_gt[i])
        for j in range(len(frames_l)):
            img_path = vid_root+'/'+objects_gt[i]+'/' + frames_l[i]
            img = image.load_img(img_path)
            x = image.img_to_array(img, )
            labels[j][x[:,:,0]!=0]=i+1
    return labels



def get_random_frames(root,videos,i,numframes,net_shape):
    frames_l = os.listdir(root+videos[i])
    t_arr = np.random.randint(1,len(frames_l),numframes)
    frames = list()
    shapes = list()
    for t in t_arr:
        img_path = root+'/'+videos[i]+'/'+frames_l[t]
        img = image.load_img(img_path)
        shapes.append(img.size)
        img = image.load_img(img_path,target_size=net_shape)
        x = image.img_to_array(img,)
        x = np.expand_dims(x, axis=0)
        x = preprocess_input(x)
        frames.append(img)
    out = {"frames":frames,"size":shapes,"t":t_arr}
    return out

def load_data(filename):
    f = open(filename,"rb")
    result = pickle.load(f)
    f.close()
    return result

def random_sample_up(fullbatch,NumSamples,optflow=False,prerandom=True):#no upsampling needed
    total = fullbatch['f'].shape[0]
    inds = np.arange(NumSamples)
    if prerandom:
        inds=np.random.permutation(total)[:NumSamples]
    result= {
        'f' : np.float32(fullbatch['f'][inds]),
        'x': fullbatch['x'][inds],
        'y': fullbatch['y'][inds],
        't': fullbatch['t'][inds],
    #    'l': fullbatch['l'][inds],
    }
    result['NormF']=fullbatch['NormF']
    result['NormXYT']=fullbatch['NormXYT']

    return result

def random_sample(vid_dict,features_dict,NumSamples,optflow=False):
    t_total = vid_dict['frames'].shape[0]
    x_total = vid_dict['shape'][1]
    y_total = vid_dict['shape'][0]
    t_arr = np.random.randint(0,t_total,NumSamples)
    t_arr.sort()
    x_arr = np.random.randint(0,x_total,NumSamples)
    y_arr = np.random.randint(0,y_total,NumSamples)
    tprev = -1
    result = {}
    result['x'] = x_arr
    result['y'] = y_arr
    result['t'] = t_arr
    if optflow:
        result['u'] = vid_dict['u'][t_arr,y_arr,x_arr]
        result['v'] = vid_dict['v'][t_arr,y_arr,x_arr]
    #result['l'] = vid_dict['l'][t_arr,y_arr,x_arr]
    layers = list(features_dict.keys())
    fl_arr =list()
    fl = 0
    fl_arr.append(fl)
    for l in layers:
        fl = fl+features_dict[l].shape[3]
        fl_arr.append(fl)
    feat = np.float32(np.zeros((y_total,x_total,fl)))
    result['f']=np.float32(np.zeros((NumSamples,fl)))
    for i in range(0,NumSamples):
        t =t_arr[i]
        y =y_arr[i]
        x =x_arr[i]

        if (t != tprev):
            tprev = t
            print('t=' + str(t) + '\n')
            for j in range(0,len(layers)):
                feat[:,:,fl_arr[j]:fl_arr[j+1]]= imresize(features_dict[layers[j]][t],output_shape=(y_total,x_total))
        result['f'][i,:]=feat[y,x,:]
    return result

def sigma_creation(batch,params,gammaF = 1, useann = True
                ,uselabelsann = False,trees=10,normalize = False):
    N = batch['f'].shape[0]
    inds = np.random.permutation(N)
    NumSamples = np.int32(params['train_pairs_frac']*N)
    inds = inds[:NumSamples]
    nn =np.int32(NumSamples/params['n_clusters'])
    F = batch['f'][inds]
    if useann:
        knnf = ann(F, nn, trees)
    else:  # Todo normal NN
        knnf = ann(F, nn, trees)
    sigmaF = obtain_sigma(F, np.int32(knnf.shape[1] - 1), knnf)
    x = np.expand_dims(batch['x'][inds],1)
    y = np.expand_dims(batch['y'][inds],1)
    t = np.expand_dims(batch['t'][inds],1)
    XYT= np.concatenate((x,y,t), axis=1)
    knnxyt = ann(XYT, nn, trees)
    sigmaXYT = obtain_sigma(XYT, np.int32(knnxyt.shape[1] - 1), knnxyt)
    return sigmaF,sigmaXYT


def build_pairs(FXYT,nn,nnforpos=None,negposrate = 2, posnumperpoint=2,useann = True
                ,uselabelsann = False,trees=10):
    nn = np.int32(nn)
    if nnforpos is not None:
        nnforpos = np.int32(nnforpos)
    pairs ={}
    knn = ann(FXYT, nn, trees)
    knn = knn[:,1:]#getting rid of the feature itself
    pos = create_positive_pairs(FXYT,knn,posnumperpoint,nnforpos)
    neg = create_negative_pairs(FXYT,knn,int(negposrate*posnumperpoint))
    Ainds = np.append(pos['A'], neg['A'])
    Binds = np.append(pos['B'], neg['B'])
    pairs['d'] = np.append(pos['d'], neg['d'])
    shuffle_inds = np.random.permutation(len(Ainds))
    Ainds = Ainds[shuffle_inds]
    Binds = Binds[shuffle_inds]
    pairs['d'] = pairs['d'][shuffle_inds]
    pairs['f'] = np.float32(np.concatenate([np.expand_dims(FXYT[Ainds],1),np.expand_dims(FXYT[Binds],1)],axis=1))
    return pairs, knn

def split_train_val(batch,fractest = 0.1):
    shuffle_inds = np.random.permutation(batch.shape[0])
    max_ind_test=np.int32(fractest*batch.shape[0])
    train = batch[max_ind_test:]
    val =  batch[:max_ind_test]
    return val,train

def ind2_xyt(ind,w,h):
    t = np.float32(np.floor(ind/w*h))
    x = (ind-t*h*w)%w
    y = np.float32(np.floor((ind -t*w*h)/w))
    return x,y,t

def create_sigma(f,voxels,frac=0.01,trees=10):
    inds = np.random.permutation(f.shape[0])
    samplnum=np.int32(len(inds)*frac)
    F=f[inds[0:samplnum]]
    nn = np.int32(samplnum/voxels)
    knn = ann(F, nn, trees)
    sigma = obtain_sigma(f, np.int32(knn.shape[1] - 1), knn)
    return sigma

def batch_to_FXYT(batch,GammaF=1):
    #if sigmaXYT is None:
    #    sigmaXYT = batch['sigmaXYT']
    #if sigmaF is None:
    #    sigmaF = batch['sigmaF']
    F = np.float32(batch['f'])
    x = np.expand_dims(batch['x'],1)
    y = np.expand_dims(batch['y'],1)
    t = np.expand_dims(batch['t'],1)
    XYT= np.concatenate((x,y,t), axis=1)
    XYT = np.float32(XYT)
    FXYT = np.concatenate((F,XYT), axis=1)
    return FXYT

def batch_normalize(batch,GammaF=1):
    F = np.float32(GammaF)*np.float32(batch['f']/batch['NormF'])
    x = np.expand_dims(batch['x'],1)
    y = np.expand_dims(batch['y'],1)
    t = np.expand_dims(batch['t'],1)
    XYT= np.float32(np.concatenate((x,y,t), axis=1)/batch['NormXYT'])
    FXYT = np.float32(np.concatenate((F,XYT), axis=1))
    return FXYT

def prepare_full_batch_features(preprocessed_data,features_dict,SigmaF=None,SigmaXYT=None,precforsigma=0.01,nnforsigma=400,GammaF=1,upsample=False,randomize = False):
    t_tot = preprocessed_data['frames'].shape[0]
    w = preprocessed_data['shape'][1]
    h = preprocessed_data['shape'][0]
    total = t_tot*w*h
    if upsample==True:
        layers = list(features_dict.keys())
        fl_arr =list()
        fl = 0
        fl_arr.append(fl)
        for l in layers:
            fl = fl+features_dict[l].shape[3]
            fl_arr.append(fl)
        feat = np.float32(np.zeros((h,w,fl)))
    else:
        fl = features_dict.shape[3]
    result = {
        'f'          : np.float32(np.zeros((total,fl))),
        'x'          : np.int32(np.zeros((total))),
        'y'          : np.int32(np.zeros((total))),
        't'          : np.int32(np.zeros((total))),
    #    'l'          : np.int32(np.zeros((total))),
        'w'          : w,
        'h'          : h,
        't_tot'       : t_tot
    }
    totalpixel_im = w*h
    pixarr = np.arange(totalpixel_im)
    x_arr = pixarr%w
    y_arr = np.int32(np.floor(pixarr/w))
    for t in np.arange(t_tot):
        if upsample:
            for j in range(0, len(layers)):
                feat[:, :, fl_arr[j]:fl_arr[j + 1]] = np.float32(imresize(features_dict[layers[j]][t], output_shape=(h, w)))
        else:
                feat = features_dict[t]
        inds = np.arange(t*w*h, (t+1)*w*h)
        result['t'][inds] = t
        result['y'][inds] = y_arr
        result['x'][inds] = x_arr
        result['f'][inds] = feat[y_arr,x_arr]
    #    result['l'][inds] = preprocessed_data['l'][t,y_arr,x_arr]
    if randomize:
        shuffle_inds = np.random.permutation(total)
        result['t'] = result['t'][shuffle_inds]
        result['y'] = result['y'][shuffle_inds]
        result['x'] = result['x'][shuffle_inds]
        result['f'] = result['f'][shuffle_inds]
    #    result['l'] = result['l'][shuffle_inds]
    result['MeanF']   = np.mean(result['f'], axis=0)
    result['StdF']     = np.std(result['f'], axis=0)
    for j in np.arange(result['f'].shape[1]):
        result['f'][:,j]=(result['f'][:,j]-result['MeanF'][j])/result['StdF'][j]
    result['NormF']   = np.float32(np.max(np.linalg.norm(result['f'], axis=1)))
    result['NormXYT'] = np.float32(np.linalg.norm([w,h,t_tot]))
    '''if SigmaF is None: #need to create sigma
        SigmaF = obtain_sigma(result['f'],nnforsigma);
    if SigmaXYT is None: #need to create sigma
        x = np.expand_dims(result['x'], 1)
        y = np.expand_dims(result['y'], 1)
        t = np.expand_dims(result['t'], 1)
        XYT = np.concatenate((x, y, t), axis=1)
        SigmaXYT =obtain_sigma(result['f'],nnforsigma);
    result['SigmaF']: SigmaF
    result['SigmaXYT']: SigmaXYT'''
    return result

'''def prepare_full_batch_features(preprocessed_data,features_dict,SigmaF=None,SigmaXYT=None,precforsigma=0.01,nnforsigma=400,GammaF=1):
    ttot = preprocessed_data['frames'].shape[0]
    w = preprocessed_data['shape'][1]
    h = preprocessed_data['shape'][0]
    total = ttot*w*h
    layers = list(features_dict.keys())
    fl_arr =list()
    fl = 0
    fl_arr.append(fl)
    for l in layers:
        fl = fl+features_dict[l].shape[3]
        fl_arr.append(fl)
    feat = np.float32(np.zeros((h,w,fl)))
    result = {
        'f'          : np.float32(np.zeros((total,fl))),
        'x'          : np.int32(np.zeros((total))),
        'y'          : np.int32(np.zeros((total))),
        't'          : np.int32(np.zeros((total))),
        'l'          : np.int32(np.zeros((total))),
        'w'          : w,
        'h'          : h,
        'ttot'       : ttot
    }
    totalpixel_im = w*h
    pixarr = np.arange(totalpixel_im)
    x_arr = pixarr%h
    y_arr = np.int32(np.floor(pixarr/w))
    for t in range(ttot):
        for j in range(0, len(layers)):
            feat[:, :, fl_arr[j]:fl_arr[j + 1]] = np.float32(imresize(features_dict[layers[j]][t], output_shape=(w, h)))
        inds = np.arange(t*w*h, (t+1)*w*h)
        result['t'][inds]=t
        result['y'][inds] = y_arr
        result['x'][inds] = x_arr
        result['f'][inds] = feat[y_arr,x_arr]
        result['l'][inds] = preprocessed_data['l'][y_arr,x_arr,t]
    if SigmaF is None: #need to create sigma
        SigmaF = obtain_sigma(result['f'],nnforsigma);
    if SigmaXYT is None: #need to create sigma
        x = np.expand_dims(result['x'], 1)
        y = np.expand_dims(result['y'], 1)
        t = np.expand_dims(result['t'], 1)
        XYT = np.concatenate((x, y, t), axis=1)
        SigmaXYT =obtain_sigma(result['f'],nnforsigma);
    result['SigmaF']: SigmaF
    result['SigmaXYT']: SigmaXYT
    return result'''




















