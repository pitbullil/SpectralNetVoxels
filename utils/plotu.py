import matplotlib.pyplot as plt
import numpy as np

def plot_loss(title,hist):
    plt.plot(hist['epoch'], hist['val_loss'], label='val loss')
    plt.plot(hist['epoch'], hist['loss'], label='train loss')
    plt.xlabel('epoch')
    plt.ylabel('loss')
    plt.title(title)
    plt.legend()
    plt.show()

def append_loss_hist(early,new):
    last = max(early['epoch'])
    early['epoch']+=[t+last+1 for t in new['epoch']]
    early['loss']+=new['loss']
    early['val_loss'] += new['val_loss']
    return early