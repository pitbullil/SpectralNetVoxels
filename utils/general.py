import pickle
import os
from skimage import io, color

def dict_save(filename,dict):
    path = filename.split('/')
    file = path[len(path)-1]
    path = path[0:len(path)-1]
    cumpath ='./'
    for dr in path:
        cumpath = cumpath+dr+'/'
        if not(os.path.isdir(cumpath)):
            os.mkdir(cumpath)
    fp = open(filename, "wb")
    pickle.dump(dict, fp,protocol=4)
    fp.close()

def dict_load(filename):
    fp = open(filename, "rb")
    result = pickle.load(fp)
    return result

def rgbtolab(image):
    lab = color.rgb2lab(image)



