import os
import numpy as np
from keras.preprocessing import image
from utils.pyflow.pyflow import coarse2fine_flow

def imdir2flow(vid_path):
    alpha = 0.012
    ratio = 0.75
    minWidth = 20
    nOuterFPIterations = 7
    nInnerFPIterations = 1
    nSORIterations = 30
    colType = 0  # 0 or default:RGB, 1:GRAY (but pass gray image with shape (h,w,1))
    result = {}
    frames_l = os.listdir(vid_path)
    frames_l.sort()
    img_path = vid_path + frames_l[0]
    curr = image.load_img(img_path)
    curr = image.img_to_array(curr, )
    curr = curr.astype(float) / 255
    h = curr.shape[0]
    w = curr.shape[1]
    t = 0
    flow_struct = {}
    flow_struct['u'] = list()
    flow_struct['v'] = list()
    for frame_s in frames_l[1:len(frames_l)]:
        img_path = vid_path + frame_s
        prev = curr
        curr = image.load_img(img_path)
        curr = image.img_to_array(curr, )
        curr = curr.astype(float) / 255
        u, v, im2W = coarse2fine_flow(
            prev, curr, alpha, ratio, minWidth, nOuterFPIterations, nInnerFPIterations,
            nSORIterations, colType)
        flow_struct['u'].append(u)
        flow_struct['u'].append(v)

    flow_struct['u'].append(u)
    flow_struct['u'] = np.concatenate(flow_struct['u'])
    flow_struct['v'].append(v)
    flow_struct['v'] = np.concatenate(flow_struct['v'])
    orig = origin_by_flow(flow_struct, h, w)
    flow_struct = expand_flow(flow_struct, orig)
    flow_struct['orig']=orig
    return flow_struct


def origin_by_flow(flow,h,w):
    #IN: flow['u'],flow['v'] -optical fields
    #    h , w image dims
    #OUT: origs['x'] origs['y'] : origin in the previous frame of pixel in the current frame
    x = np.arange(w)
    y = np.arange(h)
    xv,yv = np.meshgrid(x,y)
    xv = np.expand_dims(xv, axis=0)
    yv = np.expand_dims(yv, axis=0)
    #assuming we don't have last frame flow
    yvv = yv.repeat(flow['v'].shape[0],axis=0)
    xvv = xv.repeat(flow['u'].shape[0],axis=0)
    origx = np.round(xvv+flow['u'])
    origy = np.round(yvv+flow['v'])
    origx = np.concatenate([xv,origx],axis=0)
    origy = np.concatenate([yv,origy],axis=0)
    origx[origx>w-1] = np.nan
    origy[origy>h-1] = np.nan
    orig={}
    orig['x']=origx
    orig['y']=origy
    return orig

def expand_flow(flow,orig):
    #expands optical flow to the last frame based on naive semi constancy assumption
    last_ind=flow['u'].shape[0]
    flow_temp = {}
    flow_temp['u'] =flow['u'][last_ind-1:last_ind]
    flow_temp['v'] =flow['v'][last_ind-1:last_ind]
    orig_x =orig['x'][last_ind]
    orig_x_new = orig_x
    orig_y =orig['y'][last_ind]
    orig_y_new = orig_y
    orig_x_new[orig_x!=orig_x]=0
    orig_y_new[orig_y!=orig_y]=0
    new_u = flow_temp['u'][0][orig_y_new.astype(int),orig_x_new.astype(int)]
    new_u[np.logical_or(orig_x!=orig_x,orig_y!=orig_y)]=0
    new_u = np.expand_dims(new_u,axis = 0)
    new_v = flow_temp['v'][0][orig_y_new.astype(int),orig_x_new.astype(int)]
    new_v[np.logical_or(orig_x!=orig_x,orig_y!=orig_y)]=0
    new_v = np.expand_dims(new_v,axis = 0)

    flow['u'] = np.concatenate([flow['u'],new_u])
    flow['v'] = np.concatenate([flow['v'],new_v])
    return flow