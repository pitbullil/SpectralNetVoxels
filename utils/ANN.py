from annoy import AnnoyIndex
import numpy as np
def ann(X,k,trees):
    '''

    :param X: NxD matrix of the K features
    :param k: number of nearest neighbors
    :param trees: tree num
    :return: NxK matrix which contains the nearest neighbors list for each feature
    '''
    t = AnnoyIndex(X.shape[1])
    for i in range(0, X.shape[0]):
        t.add_item(i, X[i, :])
    t.build(trees)
    result = np.zeros([X.shape[0],k])
    for i in range(0, X.shape[0]):
        result[i,:]= t.get_nns_by_item(i,k)
    return np.int32(result)

def create_negative_pairs(F,ANN,num_per_point,labels=None):
    n = F.shape[0]
    X1 = np.repeat(np.arange(n),num_per_point)
    if labels is None:
        labels = 1+np.arange(n)
    X2 = np.random.randint(0, n, X1.shape)
    badind= np.array(np.nonzero(np.bitwise_or(np.sum(X2[:,np.newaxis] == ANN[X1],axis=1)>0,labels[X1]==labels[X2]))[0])
    #filtering background - bad indexes for negatives are only inside objects - 0 is background
    badind = badind[np.nonzero(labels[X1[badind]] != 0)[0]]
    while len(badind)>0.001*len(X1):
        Xt = X1[badind]
        posnnind = np.random.randint(0, n, Xt.shape)
        X2[badind] = posnnind
        badind = np.array(
            np.nonzero(np.bitwise_or(np.sum(X2[:, np.newaxis] == ANN[X1], axis=1) > 0, labels[X1] == labels[X2]))[0])
        badind = badind[np.nonzero(labels[X1[badind]] != 0)[0]]
    res = {}
    res['B']=X2
    res['A']=X1
    res['d']=[0]*len(X1)
    return res

def create_positive_pairs(F, ANN, num_per_point, max_NN=None,labels=None):
        if max_NN is None:
            max_NN = ANN.shape[1]
        X1 =  np.arange(F.shape[0])
        X1 =  np.repeat(X1,num_per_point)
        posnnind = np.random.randint(0, max_NN, X1.shape)
        X2 = ANN[X1, posnnind]
        if labels is not None:#semi supervised case
            good = False
            while not good:
                labels1 = labels[X1]
                labelsnn = labels[X2]
                badind = np.nonzero(np.array(labels1!=labelsnn,dtype=int))
                if(len(badind[0])>0.001*len(X1)):
                    Xt=X1[badind]
                    posnnind = np.random.randint(0, max_NN, Xt.shape)
                    X2[badind] = ANN[Xt,posnnind]
                else:
                    good = True
        res = {}
        res['A'] = X1
        res['B'] = np.int32(X2)
        res['d'] = [1] * len(X1)
        return res

def obtain_sigma(F,nn_num,ANN):
    Fn=F[ANN[:,nn_num]]
    D = F-Fn
    N = np.linalg.norm(D,axis=1)
    return np.median(N)
