import spectralvoxels as sv
from netParams import default_params
from keras.applications.vgg19 import VGG19
from feature_extract import extract_features_batch,upsampling_model
import siamesenet as sn
from core.costs import contrastive_loss
import SpectralNet as spn
import time
import os
from create_train_input import *
from sklearn.cluster import KMeans
from sklearn.preprocessing import OneHotEncoder
from core.util import get_cluster_sols
import matlab
import scipy.io as sio
import data as d
params = default_params(ev=200 ,voxels=200,dset='video',batch_size = 1024,input_shape=(67,),train_pairs_frac = 0.02,spec_ne=200,siam_ne=100,
                   n_nbrs = 3,scale_nbrs = 2,GammaF = 1,val_ratio=0.1,siamnegK=None,siamposK=15,negposrat=2
                   ,pos_per_point=2,max_pairs=500000)
dataset_dir = "../SpectralNetData/segtrack/Test/"
vids = d.list_dirs(dataset_dir+'Images/')
for vid in vids[5:6]:
    x ={'embedding': dict_load('../SpectralNetData/segtrack/Test/embeddings/100/100/'+vid+'.npy')}
    sio.matlab.savemat(dataset_dir + '/embeddings/' + str(params['n_clusters']) + '/' + str(params['n_ev']) + '/' + vid + '.mat',x)
    matlab.engine
