import keras.backend as K

from keras.models import Model, load_model,save_model
from keras.engine.training import _make_batches
from keras.layers import Input, Lambda
from keras.optimizers import RMSprop
import numpy as np
from core import train
from core import costs
from core.layer import stack_layers
from core.util import get_scale, print_accuracy, get_cluster_sols, LearningHandler, make_layer_list, train_gen, get_y_preds
from siamese import default_params
import tensorflow as tf


def create(train_data,params,weights=None):
    pairs_train = train_data['f']
    input_shape = pairs_train.shape[2:]
    siamese_inputs = {
        'A':Input(shape=input_shape,name='siamA'),
        'B':Input(shape=input_shape,name='siamB'),
    }
    # generate layers
    layers = []
    layers += make_layer_list(params['siam_arch'], 'siamese', params.get('siam_reg'))
    # compile the siamese network
    siamese_outputs = stack_layers(siamese_inputs, layers)

    # add the distance layer
    distance = Lambda(costs.euclidean_distance, output_shape=costs.eucl_dist_output_shape)(
        [siamese_outputs['A'], siamese_outputs['B']])

    # create the distance model for training
    siamese_net_distance = Model([siamese_inputs['A'], siamese_inputs['B']], distance)
    # compile the siamese network
    siamese_net_distance.compile(loss=costs.contrastive_loss, optimizer=RMSprop())
    return siamese_net_distance,siamese_outputs


def train(train_data,val_data,params,model):
    # create true y placeholder (not used in unsupervised training)
    y_true = tf.placeholder(tf.float32, shape=(None, params['n_clusters']), name='y_true')
    pairs_train = train_data['f']
    dist_train = train_data['d']
    pairs_val = val_data['f']
    dist_val = val_data['d']

    # create handler for early stopping and learning rate scheduling
    siam_lh = LearningHandler(
        lr=params['siam_lr'],
        drop=params['siam_drop'],
        lr_tensor=model.optimizer.lr,
        patience=params['siam_patience'])

    # initialize the training generator
    train_gen_ = train_gen(pairs_train, dist_train, params['siam_batch_size'])

    # format the validation data for keras
    validation_data = ([pairs_val[:, 0], pairs_val[:, 1]], dist_val)

    # compute the steps per epoch
    steps_per_epoch = int(len(pairs_train) / params['siam_batch_size'])

    # train the network
    hist = model.fit_generator(train_gen_, epochs=params['siam_ne'], validation_data=validation_data, steps_per_epoch=steps_per_epoch, callbacks=[siam_lh])

    return model,hist
    # compute the siamese embeddings of the input data
    #all_siam_preds = train.predict(siamese_outputs['A'], x_unlabeled=x_train, inputs=inputs, y_true=y_true, batch_sizes=batch_sizes)'''

def predict(predict_var, x_unlabeled, input,batch_size=4096, y_true=None,
        x_labeled=None, y_labeled=None):
    '''
    Evaluates predict_var, batchwise, over all points in x_unlabeled
    and x_labeled.

    predict_var:        list of tensors to evaluate and return
    x_unlabeled:        unlabeled input data
    inputs:             dictionary containing input_types and
                        input_placeholders as key, value pairs, respectively
    y_true:             true labels tensorflow placeholder
    batch_sizes:        dictionary containing input_types and batch_sizes as
                        key, value pairs, respectively
    x_labeled:          labeled input data
    y_labeled:          labeled input labels

    returns:    a list of length n containing the result of all tensors
                in return_var, where n = len(x_unlabeled) + len(x_labeled)
    '''

    # combined data
    if y_true is None:
        y_true = tf.placeholder(tf.float32, shape=(None, predict_var.shape.dims[1]), name='y_true')
    x = x_unlabeled
    # get shape of y_true
    y_shape = y_true.get_shape()[1:K.ndim(y_true)].as_list()

    batches = _make_batches(len(x), batch_size)

    y_preds = []
    # predict over all points
    for i, (batch_start, batch_end) in enumerate(batches):
        feed_dict = {K.learning_phase(): 0}

        # feed corresponding input for each input_type
        feed_dict[input] = x[batch_start:batch_end]

        # evaluate the batch
        y_pred_batch = np.asarray(K.get_session().run(predict_var, feed_dict=feed_dict))
        y_preds.append(y_pred_batch)

    if len(y_preds[0].shape):
        feat =  np.concatenate(y_preds)
        sigma = np.std(feat,axis=0)
        mu = np.mean(feat,axis=0)
        #for j in np.arange(feat.shape[1]):
        #    feat[:, j] = (feat[:, j] - mu[j])
        return feat
    else:
        return np.sum(y_preds)

def embed_model(siamese,layer):
    input = siamese.get_layer('siamA').input
    output = siamese.get_layer(layer).get_output_at(0)
    return Model(inputs = input,outputs = output)

def load_siam(filename):
    from keras.models import load_model
    from core.costs import contrastive_loss
    siam = load_model(filename,custom_objects={'contrastive_loss': contrastive_loss})
    return siam

