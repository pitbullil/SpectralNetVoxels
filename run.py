import spectralvoxels as sv
from netParams import default_params
from keras.applications.vgg19 import VGG19
from feature_extract import extract_features_batch,upsampling_model
import siamesenet as sn
from core.costs import contrastive_loss
import SpectralNet as spn
import time
from create_train_input import *
from sklearn.cluster import KMeans
from sklearn.preprocessing import OneHotEncoder
from core.util import get_cluster_sols
import data as d
import scipy.io as sio
import matlab.engine
ev=50
voxels=50
train_pairs_frac=0.02
dataset_dir = "../SpectralNetData/segtrack/Test/"
vids = d.list_dirs(dataset_dir+'Images/')

base_model = VGG19(weights='imagenet')
for vid in vids:
    times={
        'feat_extract' :0,
        'batch_prep': 0,
        'sigma_calc': 0,
        'fxyt': 0,
        'spectral' : 0,
        'siam_train'
        'kmeans': 0
    }

    print(vid)
    #preprocessing
    preprocess_struct = d.preprocess_single(dataset_dir, vid, (224,224))
    #extracting features
    layers = ['block2_conv2','block1_conv2'] #Layers of vgg19 for feature extraction = changing this should require change in the input
    # shape parameter as well
    start_time = time.time()
    model = upsampling_model(base_model, layers, preprocess_struct['shape'])
    feat = extract_features_batch(preprocess_struct['frames'], model)
    samp= feat.shape[0]*feat.shape[1]*feat.shape[2]*train_pairs_frac

    params = default_params(ev=ev, voxels=voxels, dset='video', batch_size=1024  , input_shape=(195,),
                            train_pairs_frac=train_pairs_frac, spec_ne=1, siam_ne=1,
                            n_nbrs=3, scale_nbrs=2, GammaF=np.float32(0.05), val_ratio=0.2, siamnegK=None, siamposK=np.int32(5),
                            negposrat=2
                            , pos_per_point=2, max_pairs=500000)
    dict_save(
        dataset_dir + '/embeddings/' + str(params['n_clusters']) + '/' + str(params['n_ev']) + '/'+vid+'_params.npy', params)

    times['feat_extract'] = time.time() - start_time
    print('Feature extraction time:'+str(times['feat_extract']))
    start_time = time.time()
    #formatting to a column
    batch = prepare_full_batch_features(preprocess_struct, feat,)
    elapsed_time = str(time.time() - start_time)
    NumSamples = np.int32(batch['h'] * batch['w'] * batch['t_tot'] * params['train_pairs_frac'])
    if (NumSamples*params['pos_per_point']*(1+params['negposrat']))>params['max_pairs']:
        NumSamples = np.int32(params['max_pairs']/(params['pos_per_point']*(1+params['negposrat'])))
    start_time = time.time()
    #sampling the batch
    batch_samp = random_sample_up(batch,NumSamples)
    elapsed_time = str(time.time() - start_time)
    print('upsample time:'+elapsed_time)
    batch_samp = batch_normalize(batch_samp,GammaF=params['GammaF'])
    batch_new = {}
    batch_new['val'],batch_new['train'] = split_train_val(batch_samp,params['val_rat'])
    del batch_samp
    start_time = time.time()
    train_pairs, train_knn = build_pairs(batch_new['train'], params['siamposK'], nnforpos=params['siamnegK'],
                                         posnumperpoint=params['pos_per_point'],negposrate=params['negposrat'])
    val_pairs, val_knn = build_pairs(batch_new['val'], params['siamposK'], nnforpos=params['siamnegK'],
                                         posnumperpoint=params['pos_per_point'],negposrate=params['negposrat'])

    #Siamese training
    siam ,siam_out= sn.create(train_pairs,params)
    siam,hist = sn.train(train_pairs,val_pairs,params,siam)
    times['siam_train'] = time.time() - start_time
    print('siamese training time:'+str(times['siam_train']))

    # Siamese embedding
    siam_out = siam.get_layer('siamese_3').get_output_at(0)
    inputsiam = siam.get_layer('siamA').input
    all_siam_preds = sn.predict(siam_out, batch_new['train'], inputsiam)
    del siam
    del val_pairs,val_knn,train_pairs, train_knn

    start_time = time.time()
    input_shape = batch_new['train'].shape[1:]
    spectral, inputs, outputs = spn.create(inputsiam, params)

    spectral, result_dict = spn.train_spec(spectral, inputs, outputs, batch_new['train'], batch_new['val'], all_siam_preds, siam_out, params)
    times['spectral_train'] = time.time() - start_time
    print('spectral training time:'+str(times['spectral_train']))

    del all_siam_preds,feat,model,batch_new
    FXYT = batch_normalize(batch)
    start_time = time.time()
    res ={
    'embed' : spn.predict(outputs['Unlabeled'],FXYT,inputs)
    }
    times['spectral_embedding'] = time.time() - start_time
    print('spectral embedding time:'+str(times['spectral_embedding']))

    np.save(dataset_dir + '/embeddings/' + str(params['n_clusters']) + '/' + str(params['n_ev']) + '/' + vid + '.npy',res['embed'])
    dict_save(dataset_dir + '/embeddings/' + str(params['n_clusters']) + '/' + str(params['n_ev']) + '/' + vid + 'times.npy',times)
    #sio.matlab.savemat(dataset_dir + '/embeddings/' + str(params['n_clusters']) + '/' + str(params['n_ev']) + '/' + vid + '.mat',res)
    '''   del spectral
    start_time = time.time()
    result={}
    result['labels'], km = get_cluster_sols(embed['embeddings'], ClusterClass=KMeans, n_clusters=params['n_clusters'],
                                            init_args={'n_init': 10})
    result['kmeans_time'] = time.time() - start_time
    print('kmeans time:' + str(result['kmeans_time']))
    
    print('OK')'''
