from keras.models import Model
from skimage.io import imsave, imread
from utils.BilinearUpSampling import *
import utils.matimresize as mr
from keras.layers import concatenate
import numpy as np

def upsampling_model(base_model,layers,finalshape):
    X = []
    for f in layers:
        feat = base_model.get_layer(f).output
        feat_up = BilinearUpSampling2D(target_size=finalshape)(feat)
        X.append(feat_up)
    if len(layers)==1:
        features = X[0]
    else:
        features = concatenate(X)
    model = Model(inputs=base_model.input, outputs=features)
    return model

def features_extractor(base_model,layers,concat=False):
    X = []
    for f in layers:
        feat = base_model.get_layer(f).output
        X.append(feat)

    if concat :
        features = concatenate(X)
    else :
        features = X
    model = Model(inputs=base_model.input,outputs=features)
    return model


def dense_features_extractor(base_model,layers,image_size):
    X = upsampling_model(base_model,layers,image_size)
    model = Model(inputs=base_model.input,outputs=X)
    return model


def extract_features_batch_lm(batch,mini_batch_size,model,image_size):
    batches = int(np.ceil(batch.shape[0]/mini_batch_size))
    image_features = list()
    for i in range(0,batches):
        mini_batch = batch[i*mini_batch_size:(i+1)*mini_batch_size]
        feats = model.predict(mini_batch)
        image_features.append(feats)
        mr.imresize(feats,(image_size[0],image_size[1]))
    result = np.concatenate(image_features)
    return image_features


def extract_features_batch(batch,model,mini_batch_size=None):
    if mini_batch_size is None:
        mini_batch_size=batch.shape[0]
    batches = np.int32(np.ceil(batch.shape[0]/mini_batch_size))
    image_features = list()
    for i in range(0,batches):
        mini_batch = batch[i*mini_batch_size:(i+1)*mini_batch_size]
        feats = model.predict(mini_batch)
        image_features.append(feats)
    if len(image_features)>1:
        image_features = np.concatenate(image_features)
    else :
        image_features = image_features[0]
    return image_features

def extract_features_mid_batch(batch,model,layers,mini_batch_size=None):
    batches = 1
    if mini_batch_size:
        batches = int(np.ceil(batch.shape[0]/mini_batch_size))
    else :
        mini_batch_size = batch.shape[0]
    image_features = {}
    mini_batch = batch[0:mini_batch_size]
    feats = model.predict(mini_batch)
    if len(layers) == 1:
        feats = [feats]
        
    for i in range(0,len(feats)):
        image_features[layers[i]] = feats[i]
    for i in range(1,batches):
        mini_batch = batch[i*mini_batch_size:(i+1)*mini_batch_size]
        feats = model.predict(mini_batch)
        for i in range(0,len(feats)):
            image_features[layers[i]]= np.concatenate([image_features[layers[i]],feats[i]])
    return image_features



