'''import sys
print(sys.version)
import data as davis
dataset_dir = "../SpectralNetData/segtrack/Train/Annotations_downsampled/"

vids = davis.list_dirs(dataset_dir)
for vid in vids:
    print('processing: '+vid)
    davis.anot_resize_and_save(dataset_dir,vid)
   '''
import data as d
import os
from scipy.misc import imsave
from keras.preprocessing import image
from utils.general import dict_load,dict_save
from utils.picutils import imresize
shape = (240,160)
dataset_dir = "../SpectralNetData/segtrack_reduced/Images/"
vid = 'monkeydog/'
root = dataset_dir+vid
frames_l = os.listdir(root)
for frame_s in frames_l:
    img_path = root  + frame_s
    img = image.load_img(img_path)
    x = image.img_to_array(img, )
    shape = img.size
    x = imresize(x, 240,160)
    #x[x >= 1] = 255
    #x[x < 1] = 0
    imsave(img_path, x)