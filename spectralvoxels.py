import siamesenet as sn
import SpectralNet as spn
import data as d
from feature_extract import extract_features_batch
from feature_extract import upsampling_model
import time

def load_spectral(data_root,vid,params):
    siam = sn.load_siam(data_root+'/siamese/'+str(params['n_clusters'])+'/'+vid+'_model.h5')
    inputsiam = siam.get_layer('siamA').input
    del siam
    spectral, inputs, outputs = spn.create(inputsiam, params)
#    spectral.load_weights(data_root+'/spectral/'+str(params['n_clusters'])+'/'+vid+str(params['n_ev'])+'_weights.npy')
    return spectral,inputs

def spectralvoxels_single(root,vid,spectralnet,spec_in,params,featurenet,feat_net_shape=(224,224),layers = ['block1_conv2'],SigmaF=None,SigmaXYT=None):
    start_time = time.time()
    preprocess_struct = d.preprocess_single(root,vid,feat_net_shape)
    times={
        'feat_extract' :0,
        'batch_prep': 0,
        'sigma_calc': 0,
        'fxyt': 0,
        'spectral' : 0,
        'kmeans': 0
    }
    start_time = time.time()
    model = upsampling_model(featurenet, layers, preprocess_struct['shape'])

    feat = extract_features_batch(preprocess_struct['frames'], model)
    times['feat_extract'] = time.time() - start_time
    print('Feature extraction time:'+str(times['feat_extract']))

    start_time = time.time()
    batch = d.prepare_full_batch_features(preprocess_struct, feat,randomize=False)
    times['batch_prep'] = time.time() - start_time
    print('Batch formation time:'+str(times['batch_prep']))

    del feat
    del featurenet
    start_time = time.time()
    #if (SigmaF is None)|(SigmaXYT is None):
    #    SigmaF,SigmaXYT = d.sigma_creation(batch,params)
    elapsed_time = time.time() - start_time
    times['sigma_calc'] = time.time() - start_time
    print('Sigma calculation time:'+str(times['sigma_calc']))
    start_time = time.time()
    FXYT = d.batch_to_FXYT(batch,SigmaF,SigmaXYT)
    times['fxyt'] = time.time() - start_time
    print('FXYT norm time:'+str(times['fxyt']))
    spectralout = spectralnet.output
    start_time = time.time()
    result = {}
    result['embeddings'] = spn.predict(spectralout,FXYT,spec_in)
    times['spectral'] = time.time() - start_time
    print('spectral embedding time:'+str(times['spectral']))
    return result,times








