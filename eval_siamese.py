from create_train_input import *
from data import *
import siamesenet as sn
import time
start_time = time.time()
from core.costs import contrastive_loss
dataset_dir = "../SpectralNetData/segtrack/Train2/"
batch = dict_load(dataset_dir+'batch/girl.npy')
all = dict_load(dataset_dir+'vgg_feats_up/girl.npy')

all['sigmaF']=batch['sigmaF']
all['sigmaXYT']=batch['sigmaXYT']

train_batch = batch_to_FXYT(all)
siam = sn.load_siam(dataset_dir+'/siamese/400/model.h5')
output = siam.get_layer('siamese_5').get_output_at(0)
#embed = sn.embed_model(siam,'siamese_5')
#embeddingsiam = embed.predict(train_batch)
input = siam.get_layer('siamA').input
embeddingsiam = sn.predict(output,train_batch,input)
print('x')
elapsed_time = str(time.time() - start_time)
print('process time:' + elapsed_time)
dict_save(dataset_dir+'siamout/girl.npy',embeddingsiam)



