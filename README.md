# Project Title

SpectralNet Adaptation to produce Super Voxels. Implemented for a final project for the course  "Nonlinear Signal Processing Using Geometric Methods (048969)"
As given by Prof. Ronen Talmon in Winter Semeter 2017/8 at the Technion, Haifa, Israel
## Prerequisites

You need to 1st fulfill al prerequisites of https://github.com/KlugerLab/SpectralNet

- scikit-learn
- tensorflow
- munkres
- annoy
- h5py

you would also need Matlab python engine installed and annoy nearest neighbors


## Running the tests

just run example.py
you might change the parameters as you like. to change architecture of the neural network change NetParams.py {x}_arch dictionary variable


## Acknowledgments

* Thanks to Uri Shacham et. al for the spectralNet idea and code. and prof. Ronen Talmon for an interesting course.
