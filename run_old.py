import spectralvoxels as sv
from netParams import default_params
from keras.applications.vgg19 import VGG19
from feature_extract import extract_features_batch, upsampling_model
import siamesenet as sn
from core.costs import contrastive_loss
import SpectralNet as spn
import time
from create_train_input import *
from sklearn.cluster import KMeans
from sklearn.preprocessing import OneHotEncoder
from core.util import get_cluster_sols
import data as d

dataset_dir = "../SpectralNetData/segtrack/Test/"
vids = d.list_dirs(dataset_dir + 'Images/')
voxels = 400
ev = 300

params = default_params(ev=ev, voxels=voxels, spec_ne=100)
base_model = VGG19(weights='imagenet')
for vid in vids:
    print(vid)
    batch = dict_load(dataset_dir + 'batch/' + vid + '.npy')
    sigmaXYT = batch['sigmaXYT']
    sigmaF = batch['sigmaF']
    x_train = batch_to_FXYT(batch['train'], sigmaF, sigmaXYT)
    x_val = batch_to_FXYT(batch['val'], sigmaF, sigmaXYT)
    del batch

    # Siamese embedding
    siam = sn.load_siam(dataset_dir + '/siamese/400/' + vid + '_model.h5')
    siam_out = siam.get_layer('siamese_3').get_output_at(0)
    inputsiam = siam.get_layer('siamA').input
    all_siam_preds = sn.predict(siam_out, x_train, inputsiam)
    del siam

    start_time = time.time()
    input_shape = x_train.shape[1:]
    spectral, inputs, outputs = spn.create(inputsiam, params)
    spectral, result_dict = spn.train_spec(spectral, inputs, outputs, x_train, x_val, all_siam_preds, siam_out, params)
    result_dict['time'] = time.time() - start_time
    print('training  time:' + str(result_dict['time']))

    dict_save(dataset_dir + '/spectral/400/' + vid + str(params['n_ev']) + '_stats.npy', result_dict)

    embed, times = sv.spectralvoxels_single(dataset_dir, vid, spectral, inputs, params, base_model, SigmaF=sigmaF,
                                            SigmaXYT=sigmaXYT)
    dict_save(dataset_dir + '/embeddings/' + str(voxels) + '/' + str(ev) + '/' + vid + '.npy', embed)
    '''   del spectral
    start_time = time.time()
    result={}
    result['labels'], km = get_cluster_sols(embed['embeddings'], ClusterClass=KMeans, n_clusters=params['n_clusters'],
                                            init_args={'n_init': 10})
    result['kmeans_time'] = time.time() - start_time
    print('kmeans time:' + str(result['kmeans_time']))

    print('OK')'''
