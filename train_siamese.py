from create_train_input import *
from core import train
import time
import matplotlib.pyplot as plt
import utils.plotu as pu
import data as d
start_time = time.time()
dataset_dir = "../SpectralNetData/segtrack/Test/"
vids = d.list_dirs(dataset_dir+'Images/')
from siamese import default_params
import siamesenet as sn


for vid in vids:
    train_pairs = dict_load(dataset_dir+'/train_pairs/'+vid+'.npy')
    val_pairs = dict_load(dataset_dir+'/val_pairs/'+vid+'.npy')

    params = default_params()
    siam , siam_out= sn.create(train_pairs,params)
    siam,hist = sn.train(train_pairs,val_pairs,params,siam)
    elapsed_time = time.time() - start_time
    print(str(elapsed_time))
    result_dict = {}
    result_dict['time'] = elapsed_time
    result_dict['loss'] = hist.history['loss']
    result_dict['val_loss'] = hist.history['val_loss']
    result_dict['epoch'] = hist.epoch
    dict_save(dataset_dir+'siamese/400/'+vid+'train_stats.npy',result_dict)
    siam.save(dataset_dir+'siamese/400/'+vid+'_model.h5')

    #pu.plot_loss('siamese loss',result_dict)
