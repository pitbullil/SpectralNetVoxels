import keras.backend as K

from keras.models import Model, load_model,save_model
from keras.engine.training import _make_batches
from keras.layers import Input, Lambda
from keras.optimizers import RMSprop
from netParams import default_params
import numpy as np
from core import train
from core import costs
from core.layer import stack_layers
from core.util import get_scale, print_accuracy, get_cluster_sols, LearningHandler, make_layer_list, train_gen, get_y_preds
import tensorflow as tf

def create(siam_in,params,weights=None):
    # generate layers
    layers = make_layer_list(params['spec_arch'][:-1], 'spectral', params.get('spec_reg'))
    layers += [
              {'type': 'tanh',
               'size': params['n_ev'],
               'l2_reg': params.get('spec_reg'),
               'name': 'spectral_{}'.format(len(params['spec_arch'])-1)},
              {'type': 'Orthonorm', 'name':'orthonorm'}
              ]
    # create spectralnet
    inputs = {
        'Unlabeled':siam_in,
        'Orthonorm': Input(shape=params['input_shape'], name='OrthonormInput'),

    }
    outputs = stack_layers(inputs, layers)
    spectral_net = Model(inputs=inputs['Unlabeled'], outputs=outputs['Unlabeled'])
    return spectral_net,inputs,outputs

def train_spec(net,inputs,outputs,x_train,x_val,siam_preds,siam_out,params):
    input_shape =  x_train.shape[1:]

    batch_sizes = {
        'Unlabeled'   :params['batch_size'],
        'Orthonorm' :params['batch_size']
    }
    y_true = tf.placeholder(tf.float32, shape=(None, params['n_clusters']), name='y_true')

    input_affinity = siam_out
    x_affinity = siam_preds
    scale = np.float32(get_scale(x_affinity, batch_sizes['Unlabeled'], params['scale_nbrs']))

    W = costs.knn_affinity(input_affinity, params['n_nbrs'], scale=scale, scale_nbr=params['scale_nbrs'])
    Dy = costs.squared_distance(outputs['Unlabeled'])
    spectral_net_loss = K.sum(W * Dy) / (2 * params['batch_size'])
    learning_rate = tf.Variable(0., name='spectral_net_learning_rate')
    train_step = tf.train.RMSPropOptimizer(learning_rate=learning_rate).minimize(spectral_net_loss, var_list=net.trainable_weights)

    # create handler for early stopping and learning rate scheduling
    spec_lh = LearningHandler(
            lr=params['spec_lr'],
            drop=params['spec_drop'],
            lr_tensor=learning_rate,
            patience=params['spec_patience'])

    hist = {
        'loss'     :list(),
        'val_loss' :list(),
        'epoch'    :np.arange(params['spec_ne'])
    }
    # begin spectralnet training loop
    spec_lh.on_train_begin()
    for i in range(params['spec_ne']):
        # train spectralnet
        hist['loss'].append(train.train_step(
                return_var=[spectral_net_loss],
                updates=net.updates + [train_step],
                x_unlabeled=x_train,
                inputs=inputs,
                y_true=y_true,
                batch_sizes=batch_sizes,
                batches_per_epoch=100)[0])

        # get validation loss
        hist['val_loss'].append(train.predict_sum(
                spectral_net_loss,
                x_unlabeled=x_val,
                inputs=inputs,
                y_true=y_true,
                batch_sizes=batch_sizes))

        # do early stopping if necessary
        '''if spec_lh.on_epoch_end(i, val_loss):
            print('STOPPING EARLY')
            break'''

        # print training status
        print("Epoch: {}, loss={:2f}, val_loss={:2f}".format(i, hist['loss'][i], hist['val_loss'][i]))

    print("finished training")
    return net,hist

def predict(predict_var, x_unlabeled, inputs,batch_size=4096, y_true=None,
        x_labeled=None, y_labeled=None):
    '''
    Evaluates predict_var, batchwise, over all points in x_unlabeled
    and x_labeled.

    predict_var:        list of tensors to evaluate and return
    x_unlabeled:        unlabeled input data
    inputs:             dictionary containing input_types and
                        input_placeholders as key, value pairs, respectively
    y_true:             true labels tensorflow placeholder
    batch_sizes:        dictionary containing input_types and batch_sizes as
                        key, value pairs, respectively
    x_labeled:          labeled input data
    y_labeled:          labeled input labels

    returns:    a list of length n containing the result of all tensors
                in return_var, where n = len(x_unlabeled) + len(x_labeled)
    '''

    # combined data
    if y_true is None:
        y_true = tf.placeholder(tf.float32, shape=(None, predict_var.shape.dims[1]), name='y_true')
    x = x_unlabeled
    # get shape of y_true
    y_shape = y_true.get_shape()[1:K.ndim(y_true)].as_list()

    batches = _make_batches(len(x), batch_size)

    y_preds = []
    # predict over all points
    for i, (batch_start, batch_end) in enumerate(batches):
        feed_dict = {K.learning_phase(): 0}
        for input_type, input_placeholder in inputs.items():
            if input_type == 'Unlabeled':
                feed_dict[input_placeholder] = x[batch_start:batch_end]
            elif input_type == 'Orthonorm':
                batch_ids = np.random.choice(len(x), size=min(len(x), batch_size), replace=False)
                feed_dict[input_placeholder] = x[batch_ids]


        # evaluate the batch
        y_pred_batch = np.asarray(K.get_session().run(predict_var, feed_dict=feed_dict))
        y_preds.append(y_pred_batch)

    if len(y_preds[0].shape):
        return np.concatenate(y_preds)
    else:
        return np.sum(y_preds)
