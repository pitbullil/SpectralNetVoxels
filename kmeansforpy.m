function R = kmeansforpy(PathIn,PathOutput,clusters,knn,w,h,t)
%KMEANSFORPY Summary of this function goes here
%   Detailed explanation goes here
E=readNPY(PathIn);
%E=load(PathIn);
%E=E.embed;
w = uint32(w);h=uint32(h);t=uint32(t);clusters=uint32(clusters);
V_total=size(E,1);
if knn
    disp('K-means + knn');
    Sample_idx = randsample(1:V_total, floor(V_total/10));
    E_sample = E(Sample_idx, :);
    opts = statset('Display', 'iter', 'MaxIter', 200,'UseParallel',0);
    [~, Cluster] = kmeans(E_sample, clusters, 'emptyaction', 'singleton','onlinephase', 'off', 'Options', opts, 'start', 'cluster');
    Group = [1:clusters]';
    mdl = fitcknn(Cluster,Group);
    IDX = int32(predict(mdl,E));
else
   disp('K-means');
   opts = statset('Display', 'iter', 'MaxIter', 200,'UseParallel',0);
   IDX = int32(kmeans(E, clusters, 'emptyaction', 'drop', 'onlinephase', 'off', 'Options', opts, 'start', 'cluster'));
end
ImSegSeq = zeros(h, w, t);
disp('Assign Colors');
Color_sample = randsample(1:255*255*255, clusters);
for i=1:clusters
    [R(i), G(i), B(i)] = V_map_3D(Color_sample(1,i),255,255);
end
ImSegSeqcolor = zeros(h, w, 3, t);

for i=1:V_total
    t_i = floor((i - 1)/(w * h)) + 1;
    y_i = floor((i - 1 - ((t_i-1) * w * h)) / w) + 1;
    x_i = mod(i - 1 - ((t_i-1) * w * h), w) + 1;
    ImSegSeqcolor(y_i, x_i, 1, t_i) = R(IDX(i));
    ImSegSeqcolor(y_i, x_i, 2, t_i) = G(IDX(i));
    ImSegSeqcolor(y_i, x_i, 3, t_i) = B(IDX(i));
    ImSegSeq(y_i, x_i, t_i) = IDX(i);
end
mkdir(PathOutput);
ImSegSeqcolor = uint8(ImSegSeqcolor);
for i=1:t
    PathSave = sprintf('%s%s%05d%s',PathOutput,'/',i,'.ppm');
    imwrite(ImSegSeqcolor(:,:,:,i), PathSave);
    PathSave = sprintf('%s%s%05d%s',PathOutput,'/',i,'.png');
    imwrite(ImSegSeqcolor(:,:,:,i), PathSave);
end
PathSave = sprintf('%s%s%05d%s',PathOutput,'/result.mat');
save(PathSave,'ImSegSeq');
R=1


