from data import *
import pickle
from utils.ANN import ann,obtain_sigmaF
from utils.general import dict_save,dict_load
import numpy as np
dataset_dir = "../SpectralNetData/segtrack/Train1"
voxels = 400
trees = 10
vids = list_dirs(dataset_dir+'/Images/')
gammaF = 1

for vid in vids:
    print('processing:'+vid+'\n')
    f = open(dataset_dir+'/vgg_feats/'+vid+'.npy','rb')
    features_dict = pickle.load(f)
    f.close()
    f = open(dataset_dir+'/preprocessed/'+vid+'.npy','rb')
    davis_vid_dict = pickle.load(f)
    shape = davis_vid_dict['shape']
    davis_vid_dict['shape'] = (int(shape[0]),int(shape[1]))
    f.close()
    NumSamples = int(davis_vid_dict['shape'][0]*davis_vid_dict['shape'][1]*len(davis_vid_dict['frames'])*0.05)
    batch = davis.random_sample(davis_vid_dict,features_dict,NumSamples)
    F = batch['feats']
    k = int(NumSamples/voxels)
    KNNF = ann(F, k, trees)
    KNNF = KNNF[:,1:]
    batch['sigmaF']= obtain_sigmaF(F,KNNF.shape[1]-1,KNNF)
    F = np.float32(F/batch['sigmaF'])
    norm = np.max(np.linalg.norm(F, axis=1))
    batch['normF']=norm
    batch['gammaF']=gammaF
    x = np.expand_dims(batch['x'],1)
    y = np.expand_dims(batch['y'],1)
    t = np.expand_dims(batch['t'],1)
    FXYT = np.concatenate((F,x,y,t), axis=1)

    knn = ann(FXYT, k, trees)
    knn = knn[:,1:]

    dict_save(dataset_dir+'/batches/'+vid+'.npy',batch)



