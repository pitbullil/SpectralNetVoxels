import sys
print(sys.version)
from utils.general import dict_save, dict_load

from keras.applications.vgg19 import VGG19
from keras.models import Model
import numpy as np
from data import *
from feature_extract import extract_features_mid_batch,features_extractor
dataset_dir = "../SpectralNetData/segtrack/Train2/"


def create_train_data_and_pairs(dataset_dir,model=None,net_shape=(224,224),base_model=None,layers=None,precentage=0.01,max_pairs=1000000
                 ,fnratio=2,voxels=400,posperpoint=2,nnmarginrate=2,val_rat=0.1):
    if model is None:
        if base_model is None:
            base_model = VGG19(weights='imagenet')
        if layers is None:
            layers = ['block1_conv2', 'block2_conv2', 'block3_conv4', 'block4_conv4']
        model = features_extractor(base_model, layers)
    preprocess_full(dataset_dir,net_shape,True)
    vids = list_dirs(dataset_dir+'Images')
    '''X0 = BilinearUpSampling2D(target_size=orig_size)(f[0])
    X1 = BilinearUpSampling2D(target_size=orig_size)(f[1])
    X2 = BilinearUpSampling2D(target_size=orig_size)(f[2])
    X3 = BilinearUpSampling2D(target_size=orig_size)(f[3])
    X =[X0,X1,X2,X3]
    Y = layers.concatenate(X)'''
    for vid in vids:
        f = open(dataset_dir + '/preprocessed/' + vid + '.npy', 'rb')
        vid_dict = pickle.load(f)
        feat = extract_features_mid_batch(vid_dict['frames'],model,layers)
        dict_save(dataset_dir+'vgg_feats/'+vid+'.npy',feat)
        shape = vid_dict['shape']
        f.close()
        NumSamples = np.int32(vid_dict['shape'][0] * vid_dict['shape'][1] * len(vid_dict['frames']) * precentage)
        if (NumSamples*posperpoint*(1+fnratio))>max_pairs:
            NumSamples = np.int32(max_pairs/(posperpoint*(1+fnratio)))
        batch = random_sample(vid_dict, feat, NumSamples)
        batch_new = {}
        batch_new['val'],batch_new['train'] = split_train_val(batch,val_rat)

        k = np.int32(NumSamples/voxels)
        train_pairs, train_knn = build_pairs(batch_new['train'], k, nnforpos=np.int32(k / nnmarginrate), posnumperpoint=posperpoint
                                           ,negposrate=fnratio)
        k = np.int32(NumSamples / voxels*val_rat)
        batch_new['sigmaF'] = train_pairs['sigmaF']
        batch_new['sigmaXYT'] = train_pairs['sigmaXYT']
        dict_save(dataset_dir + 'batch/' + vid + '.npy', batch_new)
        val_pairs, val_knn = build_pairs(batch_new['val'], k, nnforpos=np.int32(k / nnmarginrate), posnumperpoint=posperpoint
                                    ,negposrate=fnratio,sigmaF=train_pairs['sigmaF'],sigmaXYT=train_pairs['sigmaXYT'])
        dict_save(dataset_dir +'val_pairs/'+vid+'.npy',val_pairs)
        dict_save(dataset_dir + 'train_pairs/' + vid + '.npy', train_pairs)