import sys, os
# add directories in src/ to path
import argparse
from collections import defaultdict

def default_params(dset='video',train_pairs_frac = 0.02,voxels=400):
    params = defaultdict(lambda: None)

    # SET GENERAL HYPERPARAMETERS
    general_params = {
            'dset': dset,                  # dataset: reuters / mnist
            'val_set_fraction': 0.1,            # fraction of training set to use as validation
            'precomputedKNNPath': '',           # path for precomputed nearest neighbors (with indices and saved as a pickle or h5py file)
            'siam_batch_size': 4096,             # minibatch size for siamese net
            'train_pairs_frac':train_pairs_frac
            }

    params.update(general_params)
    # SET DATASET SPECIFIC HYPERPARAMETERS
    if dset == 'video':
        video_params = {
            'n_clusters': voxels,                  # number of clusters in data
            'use_code_space': True,             # enable / disable code space embedding
            'affinity': 'siamese',              # affinity type: siamese / knn
            'n_nbrs': 3,                        # number of nonzero entries (neighbors) to use for graph Laplacian affinity matrix
            'scale_nbr': 2,                     # neighbor used to determine scale of gaussian graph Laplacian; calculated by
                                                # taking median distance of the (scale_nbr)th neighbor, over a set of size batch_size
                                                # sampled from the datset

            'siam_k': 100,                        # threshold where, for all k <= siam_k closest neighbors to x_i, (x_i, k) is considered
                                                # a 'positive' pair by siamese net

            'siam_ne': 100,                       # number of training epochs for siamese net
            'siam_lr': 1e-3,                    # initial learning rate for siamese net
            'siam_patience': 10,                # early stopping patience for siamese net
            'siam_drop': 0.1,                   # learning rate scheduler decay for siamese net
            'siam_reg': None,                   # regularization parameter for siamese net
            'siam_n': None,                     # subset of the dataset used to construct training pairs for siamese net
            'siamese_tot_pairs': 1000000,        # total number of pairs for siamese net
            'arch': [                           # network architecture. if different architectures are desired for siamese net and
                                                #   spectral net, 'siam_arch' and 'spec_arch' keys can be used
                {'type': 'relu', 'size': 2048},
                {'type': 'relu', 'size': 1024},
                {'type': 'relu', 'size': 512},
                {'type': 'relu', 'size': 67},

            ],
            'use_approx': True,                # enable / disable approximate nearest neighbors
            'use_all_data': False,               # enable to use all data for training (no test set)
            }
        params.update(video_params)
        return params



