# add directories in src/ to path
from collections import defaultdict

def default_params(ev=400 ,voxels=400,dset='video',batch_size = 1024,input_shape=(67,),train_pairs_frac = 0.05,spec_ne=100,siam_ne=100,
                   n_nbrs = 20,scale_nbrs = 20,GammaF = 1,val_ratio=0.01,siamnegK=None,siamposK=None,negposrat=None
                   ,pos_per_point=None,max_pairs=250000):
    params = defaultdict()

    # SET GENERAL HYPERPARAMETERS
    general_params = {
            'dset': dset,                  # dataset: reuters / mnist
            'val_set_fraction': 0.2,            # fraction of training set to use as validation
            'precomputedKNNPath': '',           # path for precomputed nearest neighbors (with indices and saved as a pickle or h5py file)
            'siam_batch_size': 4096,             # minibatch size for siamese net
            'train_pairs_frac': train_pairs_frac,
            'siamnegK'        : siamnegK,
            'siamposK'        : siamposK,
            'siam_ne'         : siam_ne,
            'negposrat'       : negposrat,
            'pos_per_point'   : pos_per_point,
            'max_pairs'       : max_pairs,
            'siam_lr'         : 1e-3,
            'siam_patience'   : 10,
            'siam_drop': 0.1,  # learning rate scheduler decay for siamese net
            'siam_reg': None,  # regularization parameter for siamese net
            'siam_n': None,  # subset of the dataset used to construct training pairs for siamese net

    }

    params.update(general_params)
    # SET DATASET SPECIFIC HYPERPARAMETERS
    if dset == 'video':
        video_params = dict(val_rat=val_ratio, GammaF=GammaF, batch_size=batch_size, n_clusters=voxels, n_ev=ev,
                            use_code_space=True, affinity='siamese', n_nbrs=n_nbrs, scale_nbrs=scale_nbrs,
                            spec_ne=spec_ne, spec_lr=1e-3, spec_patience=20, spec_drop=0.1, spec_reg=None,
                            input_shape=input_shape, spec_arch=[
                {'type': 'relu', 'size': 4096},
                {'type': 'relu', 'size': 2048},
                {'type': 'relu', 'size': 1024},
                {'type': 'relu', 'size': input_shape[0]},
            ], siam_arch=[  # network architecture. if different architectures are desired for siamese net and
                #   spectral net, 'siam_arch' and 'spec_arch' keys can be used
                {'type': 'relu', 'size': 4096},
                {'type': 'relu', 'size': 2048},
                {'type': 'relu', 'size': 1024},
                {'type': 'relu', 'size': input_shape[0]},

            ], use_approx=True, use_all_data=False)
        params.update(video_params)
        return params



