from create_train_input import *
from core import train
import time
import matplotlib.pyplot as plt
import utils.plotu as pu
from netParams import default_params
import siamesenet as sn
from core.costs import contrastive_loss
import SpectralNet as spn
import time
import data as d
start_time = time.time()
dataset_dir ='../SpectralNetData/segtrack/Test/'
vids = ['cheetah']

for vid in vids:
    batch = dict_load(dataset_dir+'batch/'+vid+'.npy')
    sigmaXYT = batch['sigmaXYT']
    sigmaF = batch['sigmaF']
    x_train = batch_to_FXYT(batch['train'],sigmaF,sigmaXYT)
    x_val = batch_to_FXYT(batch['val'],sigmaF,sigmaXYT)
    del batch

    params = default_params()

    #Siamese embedding
    siam = sn.load_siam(dataset_dir+'/siamese/400/'+vid+'_model.h5')
    siam_out = siam.get_layer('siamese_3').get_output_at(0)
    inputsiam = siam.get_layer('siamA').input
    all_siam_preds = sn.predict(siam_out,x_train,inputsiam)
    del siam

    elapsed_time = time.time() - start_time
    print(str(elapsed_time))
    start_time = time.time()
    input_shape = x_train.shape[1:]
    spectral,inputs,outputs= spn.create(inputsiam,params)
#    spectral.load_weights(dataset_dir+'/spectral/400/'+vid+str(params['n_ev'])+'_weights.h5')
#    embeddings = spn.predict(outputs['Unlabeled'],x_train,inputs)

    spectral,result_dict = spn.train_spec(spectral,inputs,outputs,x_train,x_val,all_siam_preds,siam_out,params)
    embeddings = spn.predict(outputs['Unlabeled'],x_train,inputs)
    elapsed_time = time.time() - start_time
    result_dict['time'] = elapsed_time
    print('Trainng time:'+str(elapsed_time))

    #pu.plot_loss('spectral loss',result_dict)
    dict_save(dataset_dir+'/spectral/400/'+vid+str(params['n_ev'])+'_stats.npy',result_dict)
    spectral.save_weights(dataset_dir+'/spectral/400/'+vid+str(params['n_ev'])+'_weights.h5')


