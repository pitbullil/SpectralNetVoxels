from FastKMeans.kpp import KPP,Point
from FastKMeans.heuristic_triangleinequality import Kmeans

def Kmeansplus(k,data,kreduced,threshold=0.01):
    kplus = KPP(k, X=list(data))
    kplus.init_centers()
    cList = [Point(x, len(x)) for x in kplus.mu]
    return Kmeans(k,list(data),threshold,kreduced,cList)
