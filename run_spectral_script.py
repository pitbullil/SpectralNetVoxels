import spectralvoxels as sv
from netParams import default_params
from keras.applications.vgg19 import VGG19
from feature_extract import extract_features_batch,upsampling_model

vid = 'birdfall'
dataset_dir = "../SpectralNetData/segtrack/Test/"
voxels = 400
ev = 200
params = default_params(ev=ev,voxels=voxels)
ss,inputs = sv.load_spectral(dataset_dir,vid,params)
base_model = VGG19(weights='imagenet')

kmeans_assignments,km,times = sv.spectralvoxels_single(dataset_dir,vid,ss, inputs,params,base_model)

print('OK')


